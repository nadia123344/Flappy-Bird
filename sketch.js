var bird;
var walls;
var gameOver = false;
var level;
var restartButton = document.querySelector("#restartButton");

function setup(){
	createCanvas(400,600);
	bird = new Bird();
	walls = [];
	level=0;
}

function draw(){
	level = 0;
	background(0);
	if (frameCount % 100 ==0) {
		walls.push(new Wall());
	}
	if (gameOver){
		endGame();
	}

	for (var i = 0; i <walls.length; i++) {
		walls[i].show();
		walls[i].update();
		if (walls[i].win(bird)){
			level++;
		}
		if (bird.hit(walls[i])){
			endGame();
		}
	}

	bird.show();
	bird.update();
	makeText();

}

function keyPressed(){
	if (!gameOver &&key == ' '){
		bird.up();
	}
}

function endGame(){
	textSize(32);
	textAlign(CENTER);
	text('you lost!!!', width/2, height/2);
	noLoop();
}

function makeText(){
	textSize(15);
	textAlign(LEFT,TOP);
	fill(255,255,255);
	text('Level: '+ level,0,0);
}

restartButton.addEventListener("click",function(){
	gameOver = false;
	setup();
	loop();
})
