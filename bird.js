function Bird(){
	this.y = height/2;
	this.x = width/3;
	this.fall = 0;
	this.gravity = 0.1;
	this.radius = 8;

	this.show = function(){
		fill(255);
		ellipse(this.x, this.y, this.radius*2, this.radius*2);
	}

	this.update = function(){
		//if its at the bottom, fall or height should stop changing
		if (this.y>=height) {
			this.y=height-this.radius;
			gameOver = true;
		}
		//if its at the top, dont go up again. make the min height 0
		else if (this.y<=0) {
			this.y=1;
			this.fall=0;
		} else {
			this.fall += 1;
			this.y += this.gravity*this.fall;
		}
	}

	this.up = function(){
		if (this.fall > 0) {
			this.fall = -20;
		} else {
			this.fall -= 25;
		}
	}

	this.hit = function(wall){
		if (this.x > wall.xPos-this.radius && this.x < wall.xPos + wall.w) {
			if (this.y < wall.top + this.radius ) {
				console.log("lost, hit top wall");
				return true;
			} else if(this.y > height - wall.bottom-this.radius) {
				console.log("lost, hit bottom wall");
				return true;
			}
		}
		return false;
	}

}
