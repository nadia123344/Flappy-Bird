function Wall(){
	this.xPos = width;
	this.w = random(15, 50);
	this.bottom = random(10, height/3);
	this.top = random(10, height/3);

	this.show = function(){
		fill(255,0,0);
		rect(this.xPos, height - this.bottom, this.w, this.bottom); //bottom wall
		rect(this.xPos, 0, this.w, this.top); //top wall
	}

	this.update = function(){
		if (this.xPos > -this.w) {
			this.xPos -= 1;
		} else {
			delete(this);
		}
	}

	this.win = function(bird){
		if (this.xPos < bird.x){
			return true;
		}
	}
}
